package main

import (
	"bufio"
	"crypto/rand"
	"fmt"
	"io"
	"net"
)

var conn net.Conn

func receive(conn net.Conn) {
	reader := bufio.NewReader(conn)

	for {
		replyFromServer, _ := reader.ReadString('\n')
		fmt.Print("Reply from server: " + replyFromServer)
		fmt.Print("> ")
	}
}

func main() {

	fmt.Println("Input ip and portnumber")

	var input string
	fmt.Scanln(&input)
	clientId, _ := newUUID()

	conn, _ = net.Dial("tcp", input)

	defer conn.Close()
	go receive(conn)
	for {
		fmt.Fprintf(conn, "Hello from client"+clientId+"\n")
	}
}

func newUUID() (string, error) {
	uuid := make([]byte, 16)
	n, err := io.ReadFull(rand.Reader, uuid)
	if n != len(uuid) || err != nil {
		return "", err
	}

	uuid[8] = uuid[8]&^0xc0 | 0x80
	uuid[6] = uuid[6]&^0xf0 | 0x40
	return fmt.Sprintf("%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:]), nil
}
