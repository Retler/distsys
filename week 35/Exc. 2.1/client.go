package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
)

var conn net.Conn

func receive(conn net.Conn) {
	reader := bufio.NewReader(conn)

	for {
		replyFromServer, _ := reader.ReadString('\n')
		fmt.Print("Reply from server: " + replyFromServer)
		fmt.Print("> ")
	}
}

func main() {

	fmt.Println("Input ip and portnumber")

	var input string
	fmt.Scanln(&input)

	conn, _ = net.Dial("tcp", input)
	defer conn.Close()
	go receive(conn)
	for {
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("> ")
		text, _ := reader.ReadString('\n')
		if text == "quit\n" {
			return
		}
		fmt.Fprintf(conn, text)
	}
}
