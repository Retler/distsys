package main

import (
	"fmt"
	"net"
	"regexp"
	"sync"
	"bufio"
	"os"
	"time"
)


func validateAddress(address string) (bool, error) {
	return regexp.MatchString("[0-9]+.[0-9]+.[0-9]+.[0-9]+:[0-9]+", address)
}

type MessageMap struct{
	messagesSent map[Message]bool
	lock sync.RWMutex
}

// Messages are unique based on text, sender, and timestamp
type Message struct {
	message string
	sender string
	timestamp string
}

type Peers struct{
	peers []net.Conn
	lock sync.RWMutex
}

func MakeMessageMap() *MessageMap{
	msgMap := new(MessageMap)
	msgMap.messagesSent = make(map[Message]bool)
	return msgMap
}

func MakePeers() *Peers{
	peers := new(Peers)
	peers.peers = make([]net.Conn, 5)
	return peers
}

var messageMap = MakeMessageMap()
var messageQueue chan Message
var peers = MakePeers()

func main() {
	fmt.Println("Input ip and portnumber of peer")
	var input string
	fmt.Scanln(&input)
	messageQueue = make(chan Message)

	match, err := validateAddress(input)
	conn, connErr := net.Dial("tcp", input)
	connectionListener, _ := net.Listen("tcp", ":")

	if !match || (err != nil) || (connErr != nil) {
		// start new network
		fmt.Println("Invalid address. Starting a new network on adress: " + connectionListener.Addr().String())
		waitForFirstConnection(connectionListener)
		go listenForNewConnections(connectionListener)
	} else {
		fmt.Println("Successfully connected to peer")
		addPeer(conn)
		go handleConnection(conn)
		go listenForNewConnections(connectionListener)
	}

	go broadcast(messageQueue)

	fmt.Println("Now you can chat with peers. ")

	for{
		reader := bufio.NewReader(os.Stdin)
		message, _ := reader.ReadString('\n')
		messageQueue <- Message{message, connectionListener.Addr().String(), time.Now().String()}
	}
}

func listenForNewConnections(connectionListener net.Listener){
	fmt.Println("Listening for peers at: " + connectionListener.Addr().String())
	defer connectionListener.Close()
	for{
		conn, _ := connectionListener.Accept()
		fmt.Println("Connection from " + conn.RemoteAddr().String() + " accepted")

		addPeer(conn)
		go handleConnection(conn)
	}
}

func broadcast(c chan Message) {

	for {
		message := <-c
		okToReceiveAndSend := checkMessage(message)

		if okToReceiveAndSend {
			saveMessage(message)
			sendMessageToPeers(message)
		}
	}
}

func debug(debugMsg string){
	fmt.Println("Debugger: " + debugMsg)
}

func sendMessageToPeers(message Message){
	for _, conn := range peers.peers {

		if conn != nil && conn.RemoteAddr().String() != message.sender { // Added this check so the sender of the message doesnt get it back
			fmt.Fprintf(conn, message.message)
		}
	}
}

func handleConnection(conn net.Conn) {
	defer conn.Close()
	for {
		message, err := bufio.NewReader(conn).ReadString('\n')
		if err != nil {
			fmt.Println("A peer disconnected")
			return
		}
		fmt.Print(message)
		messageQueue <- Message{message,conn.RemoteAddr().String(), time.Now().String()}
	}
}

func waitForFirstConnection(connectionListener net.Listener){
	fmt.Println("Waiting for initial connection..")
	conn, _ := connectionListener.Accept()
	addPeer(conn)
	go handleConnection(conn)
	fmt.Println("Connection from " + conn.RemoteAddr().String() + " accepted")
}

func saveMessage(msg Message){
	messageMap.lock.Lock()
	messageMap.messagesSent[msg] = true
	messageMap.lock.Unlock()
}

func checkMessage(msg Message) bool{
	messageMap.lock.Lock()
	_, messageAlreadySent := messageMap.messagesSent[msg]
	messageMap.lock.Unlock()

	return !messageAlreadySent
}

func addPeer(conn net.Conn){
	peers.lock.Lock()
	peers.peers = append(peers.peers, conn)
	peers.lock.Unlock()
}

