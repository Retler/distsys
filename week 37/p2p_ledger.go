package main

import (
	"fmt"
	"net"
	"regexp"
	"sync"
	"bufio"
	"os"
	"strings"
	"encoding/gob"
	"io"
	"sort"
	"log"
	"time"
	"strconv"
	"crypto/rand"
)

// Method types: 0=Transaction, 1=Peer list request, 2=Presence broadcast request

func validateAddress(address string) (bool, error) {
	return regexp.MatchString("[0-9]+.[0-9]+.[0-9]+.[0-9]+:[0-9]+", address)
}

type Ledger struct {
	Accounts map[string]int
	lock sync.Mutex
}

func MakeLedger() *Ledger {
	ledger := new(Ledger)
	ledger.Accounts = make(map[string]int)
	return ledger
}

type Message struct {
	From string
	FromOrigin string
	MessageType int
	TransactionObject Transaction
	PeersFromRemote []string
	MessageId string
}

type Transaction struct {
	ID string
	From string
	To string
	Amount int
}

func (l *Ledger) Transaction(t *Transaction) {
	l.lock.Lock() ; defer l.lock.Unlock()
	l.Accounts[t.From] -= t.Amount
	l.Accounts[t.To] += t.Amount
}

type Peers struct{
	peers []net.Conn
	lock sync.RWMutex
}

func MakePeers() *Peers{
	peers := new(Peers)
	peers.peers = make([]net.Conn, 5)
	return peers
}

var ledger = MakeLedger()
var messageQueue chan Message
var peers = MakePeers()
var peerList = make([]string, 0)
var self = ""
var sentMessages = make(map[string]bool)

func main() {
	fmt.Println("Input ip and portnumber of peer")
	var input string
	fmt.Scanln(&input)
	messageQueue = make(chan Message)

	match, err := validateAddress(input)
	conn, connErr := net.Dial("tcp", input)

	var connectionListener net.Listener
	connectionListener, _ = net.Listen("tcp", ":")

	if !match || (err != nil) || (connErr != nil) { // Starting a network
		// start new network
		fmt.Println("Invalid address. Starting a new network on adress: " + connectionListener.Addr().String())
		waitForFirstConnection(connectionListener)
		go listenForNewConnections(connectionListener)
	} else { // Network exists, so just connect to it..
		self = strings.Split(conn.LocalAddr().String(),":")[0] + ":" +  strings.Split(connectionListener.Addr().String(),":")[3]
		fmt.Println("Successfully connected to peer ", conn.RemoteAddr().String())
		go handleConnection(conn)
		addPeer(conn)
		makePeerListRequest(conn)
		go listenForNewConnections(connectionListener)
	}

	go broadcast(messageQueue)

	fmt.Println("Now you can send transactions by inputting ip:portnumber. ")

	for{
		reader := bufio.NewReader(os.Stdin)
		input, _ := reader.ReadString('\n') // input has to be of form: [receiver] [ammount]
		receiver := strings.Split(input, " ")[0]
		ammount, err := strconv.Atoi(strings.TrimSuffix(strings.Split(input, " ")[1], "\r\n"))

		if err != nil { fmt.Println(err); continue }

		messageQueue <- Message{self,self, 0 ,Transaction{time.Now().String(),self,receiver,ammount}, []string{},newUUID()}
	}
}
func makePeerListRequest(conn net.Conn) {
	enc := gob.NewEncoder(conn)
	enc.Encode(Message{conn.LocalAddr().String(),self,1,Transaction{}, []string{}, newUUID()})
}

func connectToOtherPeers(peersFromRemote []string, remoteAddr string) {
	sortPeers(peersFromRemote);

	listToConnectTo := getTenPeersAfterMe(peersFromRemote, remoteAddr);

	for _, peer := range listToConnectTo{
		fmt.Println("Connecting to peer: ", peer)
		conn, connErr := net.Dial("tcp", peer)

		if connErr != nil {
			log.Fatal("Error connecting to peer " + peer)
			continue
		}

		go handleConnection(conn)

		addPeer(conn)
	}
}

func getTenPeersAfterMe(peersFromRemote []string, remoteAddr string) []string {
	result := make([]string, 0);

	for index, peer := range peersFromRemote {
		if 	peer == self{ // Find yourself on the list
			for i := 1; i < 10; i++{
				if peersFromRemote[(index+i)%len(peersFromRemote)] == self{ break }
				if peersFromRemote[(index+i)%len(peersFromRemote)] == remoteAddr { continue }
				if contains(peersFromRemote,peersFromRemote[(index+i)%len(peersFromRemote)]){ continue }
				result = append(result,peersFromRemote[(index+i)%len(peersFromRemote)])
			}
			return result;
		}
	}
	return result; // We will never get here because of the invariant that self is always in the list.
}

func contains(peerList []string, peer string) bool{
	for _,peer2 := range peerList{
		if peer2 == peer{
			return true
		}
	}
	return false
}

func sortPeers(peers []string){
	sort.Strings(peers);
}

func listenForNewConnections(connectionListener net.Listener){
	fmt.Println("Listening for peers at: " + connectionListener.Addr().String())
	defer connectionListener.Close()
	for{
		conn, _ := connectionListener.Accept()
		fmt.Println("Connection from " + conn.RemoteAddr().String() + " accepted")
		addPeer(conn)

		go handleConnection(conn)
	}
}

func broadcast(c chan Message) {

	for {
		message := <-c

		fmt.Println("Received message ", message)

		if !sentMessages[message.MessageId]{
			handleMessage(message)
		}
	}
}
func handleMessage(message Message) {
	switch message.MessageType {
	case 0:
		handleTransactionMessage(message)
	case 1:
		handlePeerListRequestMessage(message)
	case 2:
		handleBroadcastingAddressMessage(message)
	case 3:
		handlePeerListResponse(message)
	default:
		fmt.Errorf("Unsupported message type: ", message.MessageType)
	}
}

func handlePeerListResponse(message Message) {
	peerList = message.PeersFromRemote
	peerList = append(peerList,self)
	connectToOtherPeers(peerList, message.From)
	broadcastPresence()
}

func broadcastPresence() {
	for _, peer := range peers.peers {
		if peer != nil {
			if peer != nil && peer.RemoteAddr().String() != self{
				message := Message{self,self,2, Transaction{},[]string{}, newUUID()}
				enc := gob.NewEncoder(peer)
				fmt.Println("sending broadcast of presence with message: ", message)
				enc.Encode(message)
			}
		}
	}
}
func handleBroadcastingAddressMessage(message Message) {
	if message.FromOrigin != self{
		fmt.Println("Adding ", message.FromOrigin, " to list of peers and broadcasting further")
		peerList = append(peerList, message.FromOrigin)
		sendMessageToPeers(message)
	}
}
func handlePeerListRequestMessage(message Message) {
	for _, peer := range peers.peers {
		if peer != nil && peer.RemoteAddr().String() == message.From {
			msg := Message{self,self, 3, Transaction{}, peerList, newUUID()}
			enc := gob.NewEncoder(peer)
			fmt.Println("Sending message, ", msg, " to ", message.From)
			enc.Encode(msg)
		}
	}
}

func handleTransactionMessage(message Message) {
	ledger.Transaction(&message.TransactionObject)
	sendMessageToPeers(message)
	debug("Ledger is now: ")
	for x,y := range ledger.Accounts{
		debug("Account: " + x + " - Balance: " + strconv.Itoa(y))
	}
}

func debug(debugMsg string){
	fmt.Println("Debugger: " + debugMsg)
}

func sendMessageToPeers(message Message){
	for _, conn := range peers.peers {

		if (conn != nil) && !sentMessages[message.MessageId] { // Added this check so the sender of the message doesnt get it back
			message.From = self
			encoder := gob.NewEncoder(conn)
			encoder.Encode(message)
		}
	}
	sentMessages[message.MessageId] = true
}

func handleConnection(conn net.Conn) {
	debug("handling connection to: " + conn.RemoteAddr().String())
	defer conn.Close()


	for {
		msg := &Message{}
		dec := gob.NewDecoder(conn)
		err := dec.Decode(msg)

		if err == io.EOF {
			fmt.Println("Connection closed by " + conn.RemoteAddr().String())
		}

		if err != nil {
			fmt.Println(err.Error())
			return
		}

		messageQueue <- *msg
	}
}

func waitForFirstConnection(connectionListener net.Listener){
	fmt.Println("Waiting for initial connection..")
	conn, _ := connectionListener.Accept()
	self = conn.LocalAddr().String()
	peerList = append(peerList, self)
	self = conn.LocalAddr().String()
	addPeer(conn)

	go handleConnection(conn)
	fmt.Println("Connection from " + conn.RemoteAddr().String() + " accepted")
}

func addPeer(conn net.Conn){
	peers.lock.Lock()
	peers.peers = append(peers.peers, conn)
	peers.lock.Unlock()
}

// Used to generate unique id's for messages, so we can use these to create sentMessages map
func newUUID() string {
	uuid := make([]byte, 16)
	n, err := io.ReadFull(rand.Reader, uuid)
	if n != len(uuid) || err != nil {
		return ""
	}
	// variant bits; see section 4.1.1
	uuid[8] = uuid[8]&^0xc0 | 0x80
	// version 4 (pseudo-random); see section 4.1.3
	uuid[6] = uuid[6]&^0xf0 | 0x40
	return fmt.Sprintf("%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:])
}

