package main

import (
	"testing"
	"strconv"
	"math/big"
	"fmt"
)

// Check that the output key-length is correct for some possible inputs of k
func TestKeyGenLength(t *testing.T) {
	fmt.Println("Running TestKeyGenLength")
	testResults := []struct{input int; resultLength int}{
		{10,0},
		{50,0},
		{100,0},
		{1000,0},
		{2000,0},
	}

	for _,row := range testResults{
		_,_, key, _ := KeyGen(row.input)
		row.resultLength = key.BitLen()

		if(row.input != row.resultLength){
			t.Error("Key length is " + strconv.Itoa(row.resultLength) + ", should be " + strconv.Itoa(row.input))
		}else{
			fmt.Println("Tried to generate key of length", row.resultLength, "and resulting length was correct")
		}
	}
}

// In this test, we try to generate RSA keys of size 500, 1000 and 2000
// We are encrypting different messages with these keys and asserting that when we decrypt the cipher
// the result will be the original message
func TestEncryptDecrypt(t *testing.T) {
	fmt.Println("Running TestEncryptDecrypt")
	keySize := []int {500, 1000, 2000} // Key sizes must be longer than the message
	zero, _ := new(big.Int).SetString("0",10)
	one, _ := new(big.Int).SetString("1",10)
	hundred, _ := new(big.Int).SetString("100",10)
	thousand, _ := new(big.Int).SetString("1000",10)
	bignumber, _ := new(big.Int).SetString("234543235343224245435",10)
	bignumber2, _ := new(big.Int).SetString("93243549549543949323921943295439594325943958932849812394839248934893284938294893284934298429348923849",10)

	testData := []struct{
		input *big.Int
		encrypted *big.Int
		decrypted *big.Int
	}{
		{hundred,zero,one},
		{thousand, zero,one},
		{bignumber,zero,one},
		{bignumber2,zero,one},
	}

	for _, key := range keySize { // Test encryption/decryption with many different key-sizes
		p,q,n,d = KeyGen(key)

		for _, row := range testData{
			encrypted := Encrypt(*row.input)
			row.encrypted = &encrypted
			decrypted := Decrypt(encrypted)
			row.decrypted = &decrypted

			if row.input.Cmp(row.decrypted) != 0 {
				t.Error("Test failed! Input/decryption mismatch: Input: ", row.input, " Decrypted data: ", row.decrypted)
			}else{
				fmt.Println("Input value is: ", row.input, " Decrypted data: ", row.decrypted, "Success!")
			}
		}
	}
}

// In this test, we generate a bunch of messages, encrypt them using rsa with different keysizes
// and finally encrypting the result to a file, and decrypting it again. Then we try to decrypt the message
// using the decrypted RSA key and check if it succeeds
func TestEncryptDecryptFromFile(t *testing.T) {
	fmt.Println("Running TestEncryptDecryptFromFile")
	aes_key = []byte("123OfGe90ToOd4Fe"); // Just some random key
	filename := "encryptedRsaKey.txt"
	p,q,n,d = KeyGen(50)
	d_clone := d

	EncryptToFile(filename);
	d = DecryptFromFile(filename);

	if d.Cmp(&d_clone) != 0 {
		t.Error("Couldnt decrypt with key from file. Original key: ", &d_clone, ", decrypted key from file: ", &d);
	}else{
		fmt.Println("Success in decrypting key from file. Original key: ", &d_clone, ", decrypted key from file: ", &d)
	}
}
