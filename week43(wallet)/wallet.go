package main

import (
	"crypto/rand"
	"fmt"
	"math/big"
)

var file = ""
var pass = ""

func main(){
	var validpassword = "6368616e676520746869732070617373"
	var notvalidpassword = "459814e5129812459865324545891548"
	var n = Generate ("hejmeddig",validpassword)
	fmt.Println("public key is: " + n.String())
	bytemsg := []byte("GoodMessage")
	bigIntmsg := new(big.Int).SetBytes(bytemsg)
	hash := Sha256Hash(bigIntmsg.Bytes());
	hashAsBigInt := new(big.Int).SetBytes(hash[:])
	fmt.Print("hashed msg to be signed: ")
	fmt.Println(hashAsBigInt)

	//out-commented code is for testing the time to sign 16^5 messages
	var signValid = ""
	//start := time.Now()
	//for i := 0; i < 1048576; i++{
		signValid = SignMsg ("hejmeddig",validpassword,bytemsg)
	//}
	//elapsed := time.Since(start)
	//fmt.Println("Signing 1.048.576 messages took: ", elapsed.Seconds(), "seconds,")

	var signNotValid = SignMsg ("hejmeddig",notvalidpassword,bytemsg)

	valid, _ := new(big.Int).SetString(signValid,10)
	invalid, _ := new(big.Int).SetString(signNotValid,10)
	a := Encrypt(*valid)
	b := Encrypt(*invalid)
	fmt.Println("verify with valid password: " + a.String())
	fmt.Println("verify with invalid password: " + b.String())


}

func Generate(filename string, password string) (big.Int){
	file = filename
	pass = password
	var n,_ = MakePublicPrivateRsaKeys(256)
	EncryptToFile(filename, password)
	return n
}

func SignMsg(filename string, password string, msg []byte) (string) {

	//random bigInt generated, of the same length as a valid signature would be

	var bytearray = make([]byte, 32)
	rand.Read(bytearray)
	var randomNumber = new(big.Int).SetBytes(bytearray[:])
	var signature = randomNumber.String()

	//check if password and filename valid, if yes change the signature to the signed message
	if filename == file && password == pass {
		message := new(big.Int).SetBytes(msg)
		var signed =  Sign(*message)
		signature = signed.String()
	}
	return signature
}